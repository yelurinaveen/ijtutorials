package com.codeupanaya.ijttutorials.others;

import com.codeupanaya.ijtutorials.inheritance.Employee;

public class Secretary implements Employee{

	@Override
	public int getWorkHours() {
		return 48;
	}

	@Override
	public int getVacationHours() {
		return 160;
	}

	@Override
	public int getSalary() {
		return 45000;
	}

	@Override
	public String describeYourWork() {
		return "I take care of my Boss appointments";
	}
	
	
}