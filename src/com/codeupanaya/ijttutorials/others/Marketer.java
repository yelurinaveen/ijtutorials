package com.codeupanaya.ijttutorials.others;

import com.codeupanaya.ijtutorials.inheritance.Employee;

public class Marketer implements Employee{

	@Override
	public int getWorkHours() {
		return 40;
	}

	@Override
	public int getVacationHours() {
		return 160;
	}

	@Override
	public int getSalary() {
		return 55000;
	}

	@Override
	public String describeYourWork() {
		return "I Market Company in all public places";
	}
	
	
}