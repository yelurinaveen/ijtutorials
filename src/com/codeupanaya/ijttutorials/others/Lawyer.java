package com.codeupanaya.ijttutorials.others;

import com.codeupanaya.ijtutorials.inheritance.Employee;

public class Lawyer implements Employee{

	@Override
	public int getWorkHours() {
		return 48;
	}

	@Override
	public int getVacationHours() {
		return 200;
	}

	@Override
	public int getSalary() {
		return 50000;
	}

	@Override
	public String describeYourWork() {
		return "I take care of Law suits";
	}
	
	
}