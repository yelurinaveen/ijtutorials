package com.codeupanaya.ijtutorials.inheritance;

public interface Employee{
	
	public int getWorkHours();
	
	public int getVacationHours();
	
	public int getSalary();
	
	public String describeYourWork();
}