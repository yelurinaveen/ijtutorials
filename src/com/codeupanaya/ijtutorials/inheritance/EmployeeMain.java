package com.codeupanaya.ijtutorials.inheritance;

import com.codeupanaya.ijttutorials.others.Lawyer;
import com.codeupanaya.ijttutorials.others.Marketer;
import com.codeupanaya.ijttutorials.others.Secretary;

public class EmployeeMain{
	
	public static void main(String args[])	{
		Lawyer lawyerObj = new Lawyer();
		
		System.out.println(lawyerObj.getWorkHours());
		System.out.println(lawyerObj.getVacationHours());
		System.out.println(lawyerObj.getSalary());
		System.out.println(lawyerObj.describeYourWork());
		System.out.println("*****************************");
		
		Marketer marketerObj = new Marketer();
		
		System.out.println(marketerObj.getWorkHours());
		System.out.println(marketerObj.getVacationHours());
		System.out.println(marketerObj.getSalary());
		System.out.println(marketerObj.describeYourWork());
		System.out.println("*****************************");
		
		Secretary secreataryObj = new Secretary();
		
		System.out.println(secreataryObj.getWorkHours());
		System.out.println(secreataryObj.getVacationHours());
		System.out.println(secreataryObj.getSalary());
		System.out.println(secreataryObj.describeYourWork());
		System.out.println("*****************************");
	}
}